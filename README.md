# Read me first, please
This is not a representation of my skills in this framework. This is just a copy from my private Bitbucket-repo I made *months* ago. So if you see any rooky mistakes or think it is a very easy project, please take everything with a grain of salt, or a tea spoon. By any means, please don't take this as a measuring tool of what my skills are.

# Angular Clientpanel project

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.1.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

# Firebase instructions
Please add the following structure for your Firebase install (or import the database.json file)
angular client panel
|-clients
.|-[id]
..|-[balance]
..|-[email]
..|-[firstName]
..|-[lastName]
..|-[phone]
.|-[id]
..|-[balance]
..|-[email]
..|-[firstName]
..|-[lastName]
..|-[phone]
